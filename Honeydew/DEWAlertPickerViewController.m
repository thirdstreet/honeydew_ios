//
//  DEWAlertPickerViewController.m
//  Honeydew
//
//  Created by Damon Floyd on 9/10/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWAlertPickerViewController.h"
#import "DEWColors.h"

@interface DEWAlertPickerViewController ()
{
    NSArray *_alerts;
    NSUInteger _selectedIndex;
}
@end

@implementation DEWAlertPickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    _alerts = @[@"5 Minutes Before",
                @"15 Minutes Before",
                @"30 Minutes Before",
                @"1 Hour Before",
                @"2 Hours Before",
                @"1 Day Before",
                @"2 Days Before",
                @"1 Week Before"];
    
    
    _selectedIndex = [_alerts indexOfObject:self.alert];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_alerts count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AlertCell"];
    
    // Configure the cell...
    cell.textLabel.text = _alerts[indexPath.row];
    cell.textLabel.textColor = [DEWColors honeydew];
    cell.tintColor = [DEWColors cantalope];
    
    if(indexPath.row == _selectedIndex) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(_selectedIndex != NSNotFound)
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:_selectedIndex inSection:0]];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    _selectedIndex = indexPath.row;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    NSString *alert = _alerts[indexPath.row];
    [self.delegate alertPickerViewController:self didSelectAlert:alert];
}

@end
