//
//  DEWActiveCellTableViewCell.m
//  Honeydew
//
//  Created by Damon Floyd on 9/15/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWActiveCell.h"

@implementation DEWActiveCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
