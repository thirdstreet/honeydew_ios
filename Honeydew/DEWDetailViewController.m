//
//  DEWDetailViewController.m
//  Honeydew
//
//  Created by Damon Floyd on 9/24/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWDetailViewController.h"
#import "DEWStatsTableViewController.h"
#import "DEWColors.h"
#import "DEWInsultAccolade.h"
#import "YLMoment.h"

@interface DEWDetailViewController ()
{
    NSDateFormatter *_dateFormatter;
    UIColor *_c;
}
@end

@implementation DEWDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"yyyy-MM-dd @ HH:mm"];
    
    
    _c = [self.dew wasClosedLate] ? [DEWColors lateRed] : [DEWColors honeydew];
    
    self.descriptionLabel.text = self.dew.dewDescription;
    
    [self setupDewdLabels];
    
    [self setupLateLabels];
    
    [self setupCreatedLabels];
    
    [self setupDueLabels];
    
    [self setupClosedViaLabel];
    
    self.statsButton.backgroundColor = [DEWColors cantalope];
    self.statsButton.tintColor = [UIColor whiteColor];
}

-(void)setupDueLabels
{
    YLMoment *due = [[YLMoment alloc] initWithDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.dew.dueTime]];
    NSString *dueString = [due fromNow];
    NSString *dueText = [NSString stringWithFormat:@"It was due %@ on", dueString];
    NSString *fullDue = [_dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.dew.dueTime]];
    
    self.dueAgoLabel.text = dueText;
    self.dueTimestampLabel.text = fullDue;
    
    self.dueTimestampLabel.textColor = _c;
}

-(void)setupClosedViaLabel
{
    NSString *closedVia;
    if ([self.dew.closeMethod isEqualToString:@"code"]) {
        closedVia = @"entering a code";
    } else if ([self.dew.closeMethod isEqualToString:@"math"]){
        closedVia = @"being a math wizard";
    } else {
        closedVia = @"solving a puzzle";
    }
    
    NSString *closedText = [NSString stringWithFormat:@"You closed it by %@.", closedVia];
    NSMutableAttributedString *closedfullString = [[NSMutableAttributedString alloc] initWithString:closedText];
    [closedfullString addAttribute:NSForegroundColorAttributeName value:_c range:NSMakeRange(17, [closedVia length])];
    
    self.closedViaLabel.attributedText = closedfullString;
}



-(void)setupCreatedLabels
{
    YLMoment *created = [[YLMoment alloc] initWithDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.dew.createTime]];
    NSString *createdString = [created fromNow];
    NSString *createdText = [NSString stringWithFormat:@"It was created %@ on", createdString];
    NSString *fullCreated = [_dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.dew.createTime]];
    
    self.createdAgoLabel.text = createdText;
    self.createdTimestampLabel.text = fullCreated;
    
    self.createdTimestampLabel.textColor = _c;
}

- (void)setupDewdLabels
{
//    YLMoment *today = [[YLMoment alloc] initWithDate:[NSDate date]];
    YLMoment *complete = [[YLMoment alloc] initWithDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.dew.closeTime]];
    NSString *momentString = [complete fromNow];
    NSString *dewdText = [NSString stringWithFormat:@"You dew'd it %@ on", momentString];
    NSString *fullDate = [_dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:self.dew.closeTime]];
    
    self.youDidItAgoLabel.text = dewdText;
    self.didTimestampLabel.text = fullDate;
    
    self.didTimestampLabel.textColor = _c;
}

- (void)setupLateLabels
{
    NSString *lateOrOnTime = [self.dew wasClosedLate] ? @"late" : @"on-time";
    NSString *fullString = [NSString stringWithFormat:@"It was dew'd %@.", lateOrOnTime];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:fullString];
    [attrString addAttribute:NSForegroundColorAttributeName value:_c range:NSMakeRange(13, [lateOrOnTime length])];
    self.ItWasDewdLabel.attributedText = attrString;
    
    NSString *insultAccolade = [self.dew wasClosedLate] ? [DEWInsultAccolade getInsult] : [DEWInsultAccolade getAccolade];
    self.insultAccoladeLabel.text = insultAccolade;
    self.insultAccoladeLabel.textColor = _c;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)viewStats:(id)sender
{
    /*
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DEWStatsTableViewController *statsViewController = [storyboard instantiateViewControllerWithIdentifier:@"statsView"];
    statsViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    [self.navigationController presentViewController:statsViewController animated:YES completion:nil];
    */
}

@end
