//
//  DEWCreateViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/9/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DEWAlertPickerViewController.h"
#import "DEWClosePickerViewController.h"
#import "DEWNagIntervalPickerViewController.h"
#import "DEWNagEscalationPickerViewController.h"
#import "Honeydew.h"

@class DEWCreateViewController;

@protocol DEWCreateViewControllerDelegate <NSObject>

- (void)dewCreateViewController: (DEWCreateViewController *)controller didCreateDew:(Honeydew *)myDew;

@end



@interface DEWCreateViewController : UITableViewController<AlertPickerViewControllerDelegate, ClosePickerViewControllerDelegate, NagIntervalPickerViewControllerDelegate, NagEscalationPickerDelegate, UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic, weak) id<DEWCreateViewControllerDelegate> delegate;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (assign) BOOL datePickerIsShowing;
@property (weak, nonatomic) IBOutlet UITableViewCell *datePickerCell;
@property (strong, nonatomic) NSDate *dueTime;
@property (strong, nonatomic) UITextField *activeTextField;
@property (weak, nonatomic) IBOutlet UILabel *dueDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *alertLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nagIntervalLabel;
@property (weak, nonatomic) IBOutlet UILabel *escalationLabel;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;
@property (weak, nonatomic) IBOutlet UITextField *nagThreshholdTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *createButton;


- (IBAction)createNewDew:(id)sender;
- (IBAction)textFieldShouldReturn:(id)sender;

@end
