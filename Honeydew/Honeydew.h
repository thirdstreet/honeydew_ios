//
//  Honeydew.h
//  Honeydew
//
//  Created by Damon Floyd on 9/25/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Honeydew : NSManagedObject

@property (nonatomic) int16_t alert;
@property (nonatomic, retain) NSString * closeMethod;
@property (nonatomic) NSTimeInterval closeTime;
@property (nonatomic) NSTimeInterval createTime;
@property (nonatomic, retain) NSString * dewDescription;
@property (nonatomic) NSTimeInterval dueTime;
@property (nonatomic, retain) NSString * nagEscalateTo;
@property (nonatomic) int16_t nagInterval;
@property (nonatomic) int16_t nagThreshold;
@property (nonatomic, retain) NSString * notificationId;
@property (nonatomic) int16_t nagsSent;
@property (nonatomic) int16_t nagsScheduled;

- (BOOL)wasClosedLate;
- (int)lateImminentOrOkay;

@end
