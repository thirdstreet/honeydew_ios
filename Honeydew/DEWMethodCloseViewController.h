//
//  DEWMethodCloseViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/17/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DEWMethodCloseViewController;

@protocol DEWMethodCloseViewControllerDelegate <NSObject>

- (void)dewMethodViewController: (DEWMethodCloseViewController *)controller didCloseDew: (BOOL)closed;

@end

@interface DEWMethodCloseViewController : UIViewController

@property (nonatomic, weak) id<DEWMethodCloseViewControllerDelegate> delegate;

@end
