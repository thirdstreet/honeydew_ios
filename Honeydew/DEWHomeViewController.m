//
//  DEWHomeViewController.m
//  Honeydew
//
//  Created by Damon Floyd on 9/9/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWHomeViewController.h"
#import "DEWDetailViewController.h"
#import "DEWAppDelegate.h"
#import "DEWActiveCell.h"
#import "DEWRecentCell.h"
#import "DEWColors.h"
#import "YLMoment.h"

#define kRecentFetchLimit 5

@interface DEWHomeViewController ()
{
    NSMutableArray *_activeHoneydews;
    NSMutableArray *_recentHoneydews;
    NSDateFormatter *_dateFormatter;
    UIRefreshControl *_refreshControl;
    NSTimer *_refreshTimer;
}
@end

@implementation DEWHomeViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        //[self loadData];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _activeHoneydews = [[NSMutableArray alloc] init];
    _recentHoneydews = [[NSMutableArray alloc] init];
    _dateFormatter = [[NSDateFormatter alloc] init];
    _dateFormatter.dateStyle = NSDateFormatterNoStyle;
    _dateFormatter.timeStyle = NSDateFormatterMediumStyle;
    
    // DEBUG
    //[self createData];
    
    [self loadData];
    
    // this is to set header height
    self.tableView.contentInset = UIEdgeInsetsMake(-1.0f, 0.0f, 0.0f, 0.0f);
    
    //refresh control
    _refreshControl = [[UIRefreshControl alloc] init];
    [self.tableView addSubview:_refreshControl];
    [_refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    _refreshTimer = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(refreshTableTimed) userInfo:nil repeats:YES];
    
}

- (void)refreshTable
{
    [self.tableView reloadData];
    [_refreshControl endRefreshing];
}

- (void)refreshTableTimed
{
    //NSLog(@"timer fired.");
    [self.tableView reloadData];
    //[_refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

// this next method are to set the height of the top section header so there's not a weird
// gap at the top when you have Active Honeydews
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if((section == 0) && (_activeHoneydews.count != 0)) {
        return 1.0f;
    }
    return 32.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"number of sections...");
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //NSLog(@"section: %ld", (long)section);
    // Return the number of rows in the section.
    if (section == 0) {
        return _activeHoneydews.count;
    }
    return _recentHoneydews.count;

    //return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //return @"Section Name";
    
    NSString *sectionName;
    switch(section)
    {
        case 0:
            if (_activeHoneydews.count == 0) {
                sectionName = @"No Active 'dews";
            }
            else {
                sectionName = @"";
            }
            return sectionName;
            break;
        case 1:
            if(_recentHoneydews.count == 0) {
                sectionName = @"No Completed 'dews (wamp waaaaaaaaamp)";
            }
            else {
                sectionName = @"Recent Honeydews";
            }
            break;
    }
    return sectionName;
    
}

// make the header views not be in all caps
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]) {
        UITableViewHeaderFooterView *tableViewHeader = (UITableViewHeaderFooterView *)view;
        tableViewHeader.textLabel.text = [tableViewHeader.textLabel.text capitalizedString];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"section: %ld, row: %ld", (long)indexPath.section, (long)indexPath.row);
    UITableViewCell *cell;
    if (indexPath.section == 0)
    {
        DEWActiveCell *activeCell = [tableView dequeueReusableCellWithIdentifier:@"activeDewCell" forIndexPath:indexPath];
        NSManagedObject *dew = _activeHoneydews[indexPath.row];
        activeCell.descriptionLabel.text = [dew valueForKey:@"dewDescription"];
        //activeCell.dueTimeLabel.text = [_dateFormatter stringForObjectValue:[dew valueForKey:@"dueTime"]];
        activeCell.dueTimeLabel.text = [self getDueTimeStringFromDew:dew];
        activeCell.dueTimeLabel.textColor = [self getLabelColorForTime:dew];
        [activeCell.tagImageView setImage:[self getImageForDew:dew]];
        cell = activeCell;
    }
    else
    {
        DEWRecentCell *recentCell = [tableView dequeueReusableCellWithIdentifier:@"recentDewCell" forIndexPath:indexPath];
        NSManagedObject *dew = _recentHoneydews[indexPath.row];
        recentCell.descriptionLabel.text = [dew valueForKey:@"dewDescription"];
        recentCell.descriptionLabel.textColor = [DEWColors honeydew];
        [recentCell.tagImageView setImage:[UIImage imageNamed:@"happy_melon"]];
        cell = recentCell;
    }
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"ShowCreate"])
    {
        //NSLog(@"segueing to Create...");
        DEWCreateViewController *createViewController = segue.destinationViewController;
        createViewController.delegate = self;
    }
    
    if([segue.identifier isEqualToString:@"CloseDew"])
    {
        DEWCloseViewController *closeViewController = segue.destinationViewController;
        NSIndexPath *path = [self.tableView indexPathForSelectedRow];
        closeViewController.honeydew = _activeHoneydews[path.row];
        closeViewController.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"DetailView"])
    {
        DEWDetailViewController *detailViewController = segue.destinationViewController;
        NSIndexPath *path = [self.tableView indexPathForSelectedRow];
        detailViewController.dew = _recentHoneydews[path.row];
    }
}


#pragma mark Data Loading
- (void) loadData
{
    DEWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Honeydew" inManagedObjectContext:context];
    
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    [req setEntity:entity];
    
    // fetch active 'dews
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(closeTime = nil)"];
    [req setPredicate:pred];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"dueTime" ascending:YES];
    [req setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    NSManagedObject *active = nil;
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:req error:&error];
    
    if ([objects count] == 0) {
        // do nothing.  The title for the secton is handled by the table delegate above
    }
    else {
        for(int i = 0; i < [objects count]; i++)
        {
            active = objects[i];
            [_activeHoneydews addObject:active];
        }
    }
    
    pred = nil;
    pred = [NSPredicate predicateWithFormat:@"(closeTime != nil)"];
    sort = nil;
    sort = [[NSSortDescriptor alloc] initWithKey:@"closeTime" ascending:NO];
    [req setPredicate:pred];
    [req setSortDescriptors:[NSArray arrayWithObject:sort]];
    [req setFetchLimit:kRecentFetchLimit];
    
    NSManagedObject *recent = nil;
    objects = nil;
    objects = [context executeFetchRequest:req error:&error];
    if ([objects count] == 0) {
        // do nothing.  The section header retitle is handled by the table delegate above
    }
    else {
        for (int i = 0; i < [objects count]; i++)
        {
            recent = objects[i];
            [_recentHoneydews addObject:recent];
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - Close Controller Delegate
- (void)dewCloseViewController:(DEWCloseViewController *)controller didCloseDew:(BOOL)closed
{
    [[self navigationController] popViewControllerAnimated:YES];
    [_activeHoneydews removeAllObjects];
    [_recentHoneydews removeAllObjects];
    
    [self loadData];
    //[self.tableView reloadData];
}

#pragma mark - Create Controller Delegate
- (void)dewCreateViewController:(DEWCreateViewController *)controller didCreateDew:(Honeydew *)myDew
{
    //NSLog(@"called as delegate..");
    DEWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    [context save:&error];
    
    [[self navigationController] popViewControllerAnimated:YES];
    
    // add the new dew to the active array:
    [_activeHoneydews addObject:myDew];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"dueTime" ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObject:sort];
    _activeHoneydews = [[_activeHoneydews sortedArrayUsingDescriptors:descriptors] mutableCopy];
    
    [self.tableView reloadData];
}

#pragma mark - Utility Methods

// get correct time string based on a few conditions
- (NSString *)getDueTimeStringFromDew: (NSManagedObject *)dew
{
    // get the stuff we need...
    NSDate *dueTime = [dew valueForKey:@"dueTime"];
    NSDate *now = [NSDate date];
    NSNumber *alertSeconds = [dew valueForKey:@"alert"];
    alertSeconds = @([alertSeconds integerValue] * 60);
    
    NSTimeInterval difference = [now timeIntervalSinceDate:dueTime];
    
    long late = [alertSeconds longValue] + difference;
    //NSLog(@"%@ + %f = %ld", alertSeconds, difference, late);
    
    if(late > [alertSeconds longValue]) {
        // it's late
        //NSLog(@"it's late...");
        return [self getMomentStringFromDate:dueTime];
        
    }
    else if(late == [alertSeconds longValue]) {
        // unlikely, but still...
        return @"Right NOW!!";
    }
    //NSLog(@"not late yet...");
    // by default, return a moment string
    return [self getMomentStringFromDate:dueTime];
}

// convert time/date to a moment-style string "3 minutes ago", "3 days from now", etc..
- (NSString *)getMomentStringFromDate: (NSDate *)date
{
    YLMoment *moment = [YLMoment momentWithDate:date];
    return [moment fromNow];
}

- (UIColor *)getLabelColorForTime:(NSManagedObject *)dew;
{
    int late = [self dewisLateOrNot:dew];
    if(late == 1) {
        return [DEWColors lateRed];
    }
    
    if(late == 0) {
        return [DEWColors cantalope];
    }
    
    return [DEWColors honeydew];
}

- (UIImage *)getImageForDew:(NSManagedObject *)dew
{
    int late = [self dewisLateOrNot:dew];
    if (late == 1) {
        return [UIImage imageNamed:@"angry_melon"];
    }
    
    if(late == 0) {
        return [UIImage imageNamed:@"nervous_melon"];
    }
    
    return [UIImage imageNamed:@"happy_melon"];
}

// returns:
//       1 if late
//       0 if imminent
//      -1 if okay
- (int)dewisLateOrNot:(NSManagedObject *)dew
{
    NSDate *dueTime = [dew valueForKey:@"dueTime"];
    long alertSeconds = [[dew valueForKey:@"alert"] longValue] * 60;
    NSDate *now = [NSDate date];
    NSTimeInterval difference = [now timeIntervalSinceDate:dueTime];
    
    long late = alertSeconds + difference;
    if(late > alertSeconds) {
        return 1;
    }
    
    if((late > 0) && (late < alertSeconds)) {
        // IMMINENT!!
        return 0;
    }
    
    // we're good
    return -1;
}

// create tasks for debug purposes
- (void) createData
{
    DEWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSManagedObject *dew;
    dew = [NSEntityDescription insertNewObjectForEntityForName:@"Honeydew" inManagedObjectContext:context];
    [dew setValue:@"This is imminent!!!" forKey:@"dewDescription"];
    [dew setValue:[NSNumber numberWithInt:30] forKey:@"alert"];
    [dew setValue:@"math" forKey:@"closeMethod"];
    [dew setValue:nil forKey:@"closeTime"];
    [dew setValue:[NSDate date] forKey:@"createTime"];
    [dew setValue:@"fussy" forKey:@"nagEscalateTo"];
    NSDate *today = [NSDate date];
    [dew setValue:[today dateByAddingTimeInterval:60 * 20] forKey:@"dueTime"];
    [dew setValue:[NSNumber numberWithInt:10] forKey:@"nagInterval"];
    [dew setValue:[NSNumber numberWithInt:5] forKey:@"nagThreshold"];
    
    NSError *error;
    [context save:&error];
    
    dew = [NSEntityDescription insertNewObjectForEntityForName:@"Honeydew" inManagedObjectContext:context];
    [dew setValue:@"Get dry cleaning" forKey:@"dewDescription"];
    [dew setValue:[NSNumber numberWithInt:10] forKey:@"alert"];
    [dew setValue:@"code" forKey:@"closeMethod"];
    [dew setValue:[NSDate date] forKey:@"createTime"];
    [dew setValue:@"insistent" forKey:@"nagEscalateTo"];
    //NSDate *today = [NSDate date];
    [dew setValue:[today dateByAddingTimeInterval:-60 * 30] forKey:@"closeTime"];
    [dew setValue:[today dateByAddingTimeInterval:60 * 120] forKey:@"dueTime"];
    [dew setValue:[NSNumber numberWithInt:10] forKey:@"nagInterval"];
    [dew setValue:[NSNumber numberWithInt:5] forKey:@"nagThreshold"];
    
    [context save:&error];
    
}

@end
