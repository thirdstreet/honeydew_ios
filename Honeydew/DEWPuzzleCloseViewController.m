//
//  DEWPuzzleCloseViewController.m
//  Honeydew
//
//  Created by Damon Floyd on 10/17/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWColors.h"
#import "DEWPuzzleCloseViewController.h"

@interface DEWPuzzleCloseViewController ()
{
    NSArray *_colors;
    NSMutableArray *_puzzleButtons;
    NSMutableArray *_puzzle;
    NSMutableArray *_solutionButtons;
    NSMutableArray *_solution;
}
@end

@implementation DEWPuzzleCloseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupColors];
    [self setupPuzzle];
    
    self.closeButton.backgroundColor = [DEWColors honeydew];
    self.closeButton.tintColor = [UIColor whiteColor];
    
    self.cancelButton.backgroundColor = [DEWColors reallyLightGray];
    self.cancelButton.tintColor = [UIColor blackColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup
- (void)setupColors
{
    UIColor *c0 = [UIColor colorWithRed:0.29 green:0.992 blue:0.945 alpha:1]; /*#4afdf1*/
    UIColor *c1 = [UIColor colorWithRed:0.686 green:0.988 blue:0.29 alpha:1]; /*#affc4a*/
    UIColor *c2 = [UIColor colorWithRed:0.592 green:0.29 blue:0.988 alpha:1]; /*#974afc*/
    UIColor *c3 = [UIColor colorWithRed:0.988 green:0.29 blue:0.337 alpha:1]; /*#fc4a56*/
    
    _colors = [NSArray arrayWithObjects:c0, c1, c2, c3, nil];
}


- (void)setupPuzzle
{
    _puzzleButtons = [[NSMutableArray alloc] initWithObjects:self.puzzle0, self.puzzle1, self.puzzle2, self.puzzle3, nil];
    _solutionButtons = [[NSMutableArray alloc] initWithObjects:self.solution0, self.solution1, self.solution2, self.solution3, nil];
    _puzzle = [NSMutableArray arrayWithCapacity:4];
    _solution = [NSMutableArray arrayWithCapacity:4];
    
    for (NSInteger i = 0; i < [_solutionButtons count]; i++) {
        NSInteger colorIndex = arc4random() % 4;
        UIButton *btn = [_solutionButtons objectAtIndex:i];
        btn.backgroundColor = _colors[colorIndex];
        [_solution addObject:[NSString stringWithFormat:@"%ld", (long)colorIndex]];
    }
    
    for (UIButton *b in _puzzleButtons) {
        b.backgroundColor = _colors[0];
        [_puzzle addObject:[NSString stringWithFormat:@"%ld", (long)0]];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)puzzleButtonTap:(UIButton *)sender
{
    NSInteger btnIndex = 0;
    if (sender == self.puzzle1) {
        btnIndex = 1;
    } else if (sender == self.puzzle2) {
        btnIndex = 2;
    } else if (sender == self.puzzle3) {
        btnIndex = 3;
    }
    
    NSString *colorString = [_puzzle objectAtIndex:btnIndex];
    NSInteger colorIndex = [colorString integerValue];
    colorIndex++;
    colorIndex = colorIndex % 4;
    colorString = [NSString stringWithFormat:@"%ld", (long)colorIndex];
    [_puzzle replaceObjectAtIndex:btnIndex withObject:colorString];
    sender.backgroundColor = _colors[colorIndex];
}

- (IBAction)closeIt:(id)sender
{
    BOOL allMatch = YES;
    for (NSInteger i = 0; i < _puzzle.count; i++) {
        NSString *solutionValue = [_solution objectAtIndex:i];
        NSString *puzzleValue = [_puzzle objectAtIndex:i];
        if (![puzzleValue isEqualToString:solutionValue]) {
            allMatch = NO;
            continue;
        }
    }
    
    if (allMatch) {
        [self.delegate dewMethodViewController:self didCloseDew:YES];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Um. No." message:@"Those don't match." delegate:self cancelButtonTitle:@"Oh My Bad" otherButtonTitles: nil];
        [alert show];
    }
}

- (IBAction)cancelClose:(id)sender
{
    [self.delegate dewMethodViewController:self didCloseDew:NO];
}


#pragma mark - Helpers

- (void)shuffleArray: (NSMutableArray *)array
{
    NSUInteger count = [array count];
    for (NSUInteger i = 0; i < count; i++) {
        NSInteger remain = count - i;
        NSInteger exchange = i + arc4random_uniform((unsigned int)remain);
        [array exchangeObjectAtIndex:i withObjectAtIndex:exchange];
    }
}

@end
