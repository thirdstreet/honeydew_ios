//
//  DEWCreateViewController.m
//  Honeydew
//
//  Created by Damon Floyd on 9/9/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DEWCreateViewController.h"
#import "DEWColors.h"
#import "DEWAppDelegate.h"
#import "DEWSoundPack.h"

#define datePickerIndex 2
#define datePickerCellHeight 164
#define MINUTE 60
#define HOUR 60
#define ESCALATION_LIMIT 100

@interface DEWCreateViewController ()
{
    NSString *_alert;
    NSString *_closeMethod;
    NSString *_nagInterval;
    NSString *_nagEscalationLevel;
    Honeydew *_dew;
    
    NSDictionary *_alertValues;
    NSDictionary *_closeMethodValues;
    NSDictionary *_nagIntervalValues;
    NSDictionary *_nagEscalationValues;
    UIRefreshControl *_refreshControl;
    
}
@end

@implementation DEWCreateViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if((self = [super initWithCoder:aDecoder]))
    {
        _alert = @"5 Minutes Before";
        _closeMethod = @"Code";
        _nagInterval = @"5 Minutes";
        _nagEscalationLevel = @"Firmer";
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupLabels];
    
    [self signUpForKeyboardNotifications];
    
    [self hideDatePickerCell];
    
    [self setupValues];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.tableView addGestureRecognizer:tapRecognizer];
    
}



- (void)setupLabels
{
    // textbox borders
    self.descriptionTextField.layer.cornerRadius = 8.0f;
    self.descriptionTextField.layer.masksToBounds = YES;
    self.descriptionTextField.layer.borderColor = [[DEWColors honeydew] CGColor];
    self.descriptionTextField.layer.borderWidth = 1.0f;
    
    self.nagThreshholdTextField.layer.cornerRadius = 8.0f;
    self.nagThreshholdTextField.layer.masksToBounds = YES;
    self.nagThreshholdTextField.layer.borderColor = [[DEWColors honeydew] CGColor];
    self.nagThreshholdTextField.layer.borderWidth = 1.0f;
    
    [self.nagThreshholdTextField setDelegate:self];
    
    // labels for picker fields
    self.alertLabel.text = _alert;
    self.alertLabel.textColor = [DEWColors honeydew];
    
    self.closeLabel.text = _closeMethod;
    self.closeLabel.textColor = [DEWColors honeydew];
    
    self.nagIntervalLabel.text = _nagInterval;
    self.nagIntervalLabel.textColor = [DEWColors honeydew];
    
    self.escalationLabel.text = _nagEscalationLevel;
    self.escalationLabel.textColor = [DEWColors honeydew];
    
    //due date
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
    NSDate *today = [NSDate date];
    // default date/time due is 30 minutes from RIGHT NOW
    NSDate *defaultDueDate = [today dateByAddingTimeInterval:(60 * 30)];
    
    self.dueDateLabel.text = [self.dateFormatter stringFromDate:defaultDueDate];
    self.dueDateLabel.textColor = [DEWColors honeydew];
    self.dueTime = defaultDueDate;
    
    self.createButton.tintColor = [DEWColors cantalope];
    
    // comment this out if you are testing and need to create a 'dew which is already late
    // by specifying a due date in the past.
    self.datePicker.minimumDate = [NSDate date];
}


- (void)setupValues
{
    NSArray *keys = @[@"5 Minutes Before",
                           @"15 Minutes Before",
                           @"30 Minutes Before",
                           @"1 Hour Before",
                           @"2 Hours Before",
                           @"1 Day Before",
                           @"2 Days Before",
                           @"1 Week Before"];
    
    NSArray *values = @[@5, @15, @30, @60, @120, @(24 * 60), @(48 * 60), @(7 * 24 * 60)];
    _alertValues = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    keys = @[@"Code", @"Math Problem", @"Puzzle"];
    values = @[@"code", @"math", @"puzzle"];
    
    _closeMethodValues = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    keys = @[@"5 Minutes", @"10 Minutes", @"15 Minutes", @"30 Minutes", @"Hour"];
    values = @[@5, @10, @15, @30, @60];
    
    _nagIntervalValues = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    keys = @[@"Firmer", @"Insistent", @"Fussy", @"Angry"];
    values = @[@"firmer", @"insistent", @"fussy", @"angry"];
    
    _nagEscalationValues = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
}

- (void)dismissKeyboard
{
    //NSLog(@"dismiss Keyboard.");
    [self.descriptionTextField resignFirstResponder];
    [self.nagThreshholdTextField resignFirstResponder];
}

- (void)signUpForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
}

- (IBAction)textFieldShouldReturn:(id)sender
{
    [sender resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    if (self.datePickerIsShowing) {
        [self hideDatePickerCell];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//dismiss keyboard when any other field is touched.
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [[event allTouches] anyObject];
    if([self.descriptionTextField isFirstResponder] && [touch view] != self.descriptionTextField) {
        [self.descriptionTextField resignFirstResponder];
    }
    
    if([self.nagThreshholdTextField isFirstResponder] && [touch view] != self.nagThreshholdTextField) {
        [self.nagThreshholdTextField resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - Table view Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = self.tableView.rowHeight;
    if(indexPath.row == datePickerIndex)
    {
        height = self.datePickerIsShowing ? datePickerCellHeight : 0.0f;
    }
    
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 1)
    {
        if(self.datePickerIsShowing)
        {
            [self hideDatePickerCell];
        }
        else
        {
            [self.activeTextField resignFirstResponder];
            [self showDatePickerCell];
        }
    }
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)showDatePickerCell
{
    //NSLog(@"showDatePickerCell");
    self.datePicker.date = self.dueTime;
    self.datePickerIsShowing = YES;
    [self.tableView beginUpdates];
    
    [self.tableView endUpdates];
    
    self.datePicker.hidden = NO;
    self.datePicker.alpha = 0.0f;
    
    
    [UIView animateWithDuration:0.25 animations:^{
        self.datePicker.alpha = 1.0f;
    }];
    
}

- (void)hideDatePickerCell
{
    //NSLog(@"hideDatePickerCell");
    self.datePickerIsShowing = NO;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.datePicker.alpha = 1.0;
                     }
                     completion:^(BOOL finished) {
                         self.datePicker.hidden = YES;
                     }];
     
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 7;
}

#pragma mark - UITextField delegate methods
- (void)textFieldDidBeginEditing: (UITextField *)textField
{
    //NSLog(@"textFieldDidBeginEditing");
    self.activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.tag == 100) {
        // allow backspace
        if(!string.length) {
            return YES;
        }
        
        NSString *newVal = [NSString stringWithFormat:@"%@%@", textField.text, string];
        int n = [newVal intValue];
        //NSLog(@"%d", n);
        
        // make sure value does not exceed 20
        if (n > 20) {
            //NSLog(@"greater than 20");
            return NO;
        }
        
        // allow integers
        if(([string intValue]) || ([string isEqualToString:@"0"]))
        {
            //NSLog(@"allow integers...");
            return YES;
        }
    }
    //NSLog(@"NO SOUP WITH BUFFET!");
    return NO;
    
}

#pragma mark - Action Methods
- (IBAction)pickerDateChanged:(UIDatePicker *)sender
{
    //NSLog(@"%@", [self.dateFormatter stringFromDate:sender.date]);
    self.dueDateLabel.text = [self.dateFormatter stringFromDate:sender.date];
    self.dueTime = sender.date;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"PickAlert"])
    {
        //NSLog(@"segueing to Alert...");
        DEWAlertPickerViewController *alertPicker = segue.destinationViewController;
        alertPicker.delegate = self;
        alertPicker.alert = _alert;
    }
    
    if([segue.identifier isEqualToString:@"PickCloseMethod"])
    {
        DEWClosePickerViewController *closePicker = segue.destinationViewController;
        closePicker.delegate = self;
        closePicker.closeMethod = _closeMethod;
    }
    
    if([segue.identifier isEqualToString:@"PickNagInterval"])
    {
        DEWNagIntervalPickerViewController *nagIntervalPicker = segue.destinationViewController;
        nagIntervalPicker.delegate = self;
        nagIntervalPicker.nagInterval = _nagInterval;
    }
    
    if([segue.identifier isEqualToString:@"PickEscalationLevel"])
    {
        DEWNagEscalationPickerViewController *escalationPicker = segue.destinationViewController;
        escalationPicker.delegate = self;
        escalationPicker.nagEscalationLevel = _nagEscalationLevel;
        
    }
}

#pragma mark - Delegate Methods
- (void)alertPickerViewController:(DEWAlertPickerViewController *)controller didSelectAlert:(NSString *)alert
{
    //NSLog(@"alert delegate!");
    _alert = alert;
    self.alertLabel.text = _alert;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)closeMethodPickerViewController:(DEWClosePickerViewController *)controller didSelectCloseMethod:(NSString *)closeMethod
{
//    NSLog(@"%@", closeMethod);
//    if ([closeMethod isEqualToString:@"Puzzle"]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Don't" message:@"Puzzle doesn't work yet. Sorry." delegate:nil cancelButtonTitle:@"k." otherButtonTitles:nil];
//        [alert show];
//    }
//    else {
        _closeMethod = closeMethod;
        self.closeLabel.text = _closeMethod;
    
    //}
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)nagIntervalPickerController:(DEWNagIntervalPickerViewController *)controller didSelectNagInterval:(NSString *)nagInterval
{
    _nagInterval = nagInterval;
    self.nagIntervalLabel.text = _nagInterval;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)nagEscalationPickerController:(DEWNagEscalationPickerViewController *)controller didSelectEscalationLevel:(NSString *)escalationLevel
{
    _nagEscalationLevel = escalationLevel;
    self.escalationLabel.text = _nagEscalationLevel;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (int)scheduleNotifications:(Honeydew *)dew
{
    int totalScheduled = 0;
    DEWSoundPack *soundPack = [[DEWSoundPack alloc] init];
    [soundPack loadWithName:@"Defaults"];
    
    NSDate *dueDate = [NSDate dateWithTimeIntervalSinceReferenceDate:dew.dueTime];
    
    // schedule the first alert
    UILocalNotification *preAlert = [[UILocalNotification alloc] init];
    preAlert.alertBody = [NSString stringWithFormat:@"Soon!: %@", dew.dewDescription];
    preAlert.fireDate = [NSDate dateWithTimeInterval:-(60 * dew.alert) sinceDate:dueDate];
    preAlert.soundName = [soundPack.sounds valueForKey:@"alert"];
    preAlert.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:dew.notificationId, @"id", nil];
    [[UIApplication sharedApplication] scheduleLocalNotification:preAlert];
    
    
    // schedule the nags
    UILocalNotification *nag = [[UILocalNotification alloc] init];
    nag.alertBody = [NSString stringWithFormat:@"Overdue!: %@", dew.dewDescription];
    nag.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:dew.notificationId, @"id", nil];
    nag.soundName = [soundPack.sounds valueForKey:@"due"];
    
    for(int i = 0; i < dew.nagThreshold; i++)
    {
        nag.fireDate = [NSDate dateWithTimeInterval:((i * dew.nagInterval) * MINUTE) sinceDate:dueDate];
        [[UIApplication sharedApplication] scheduleLocalNotification:nag];
        totalScheduled++;
    }
    
    //NSLog(@"last firedate: %@", [_dateFormatter stringFromDate:nag.fireDate]);
    
    // schedule the escalations...
    NSDate *firstFireDateAfterEscalate = [NSDate dateWithTimeInterval:((dew.nagThreshold * dew.nagInterval) * MINUTE) sinceDate:dueDate];
    UILocalNotification *reallyNag = [[UILocalNotification alloc] init];
    reallyNag.alertBody = [NSString stringWithFormat:@"NAG!: %@", dew.dewDescription];
    reallyNag.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:dew.notificationId, @"id", nil];
    reallyNag.soundName = [soundPack.sounds valueForKey:dew.nagEscalateTo];
    NSDate *twentyFourHoursFromDue = [NSDate dateWithTimeInterval:(MINUTE * HOUR * 24) sinceDate:dueDate];
    NSTimeInterval ti24 = [twentyFourHoursFromDue timeIntervalSinceReferenceDate];
    NSTimeInterval start = [firstFireDateAfterEscalate timeIntervalSinceReferenceDate];
    
    int scheduled = 0;
    //determine how many escalations to schedule:
    int escalationCount = ((ti24 - start) / (dew.nagInterval * MINUTE));
    //NSLog(@"esc count: %d = (%f - %f) / (%d * %d)", escalationCount, ti24, start, dew.nagInterval, MINUTE);
    if (escalationCount > ESCALATION_LIMIT) {
        escalationCount = ESCALATION_LIMIT;
    }
    
    for (int j = 0; j < escalationCount; j++) {
        NSDate *next = [firstFireDateAfterEscalate dateByAddingTimeInterval:((j * dew.nagInterval) * MINUTE)];
        reallyNag.fireDate = next;
        [[UIApplication sharedApplication] scheduleLocalNotification:reallyNag];
        scheduled++;
    }
    
    
    totalScheduled += scheduled;
    return totalScheduled;
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    // create button
    if(buttonIndex == 1) {
        [self saveDew];
    }
}



- (void)saveDew {
    DEWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *ent = [NSEntityDescription entityForName:@"Honeydew" inManagedObjectContext:context];
    _dew = [[Honeydew alloc] initWithEntity:ent insertIntoManagedObjectContext:context];
    
    [_dew setValue:[_alertValues valueForKey:_alert] forKey:@"alert"];
    [_dew setValue:[_closeMethodValues valueForKey:_closeMethod] forKey:@"closeMethod"];
    [_dew setValue:nil forKey:@"closeTime"];
    [_dew setValue:[NSDate date] forKey:@"createTime"];
    [_dew setValue:_descriptionTextField.text forKey:@"dewDescription"];
    [_dew setValue:_dueTime forKey:@"dueTime"];
    [_dew setValue:[_nagEscalationValues valueForKey:_nagEscalationLevel] forKey:@"nagEscalateTo"];
    [_dew setValue:[_nagIntervalValues valueForKey:_nagInterval] forKey:@"nagInterval"];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *threshhold = [formatter numberFromString:_nagThreshholdTextField.text];
    [_dew setValue:threshhold forKey:@"nagThreshold"];
    [_dew setValue:[[NSUUID UUID] UUIDString] forKey:@"notificationId"];
    _dew.nagsScheduled = [self scheduleNotifications:_dew];
    
    //NSLog(@"scheduled: %d", _dew.nagsScheduled);
    _dew.nagsSent = 0;
    [self.delegate dewCreateViewController:self didCreateDew:_dew];
    
}

- (IBAction)createNewDew:(id)sender {
    if(_descriptionTextField.text.length > 5) {
        UIAlertView *confirm = [[UIAlertView alloc] initWithTitle:@"Confirm"
                                                        message:@"Create new Honeydew?"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel" otherButtonTitles:@"Create It", nil];
        [confirm show];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Way to go..." message:@"You need to say what you want to 'dew.  5 chars at least."
                                                       delegate:nil
                                              cancelButtonTitle:@"My Bad"
                                              otherButtonTitles:nil];
        
        [alert show];
    }
}
@end
