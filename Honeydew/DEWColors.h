//
//  DEWColors.h
//  Honeydew
//
//  Created by Damon Floyd on 9/9/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DEWColors : NSObject

+ (UIColor *) honeydew;
+ (UIColor *) cantalope;
+ (UIColor *) lateRed;
+ (UIColor *) reallyLightGray;

@end
