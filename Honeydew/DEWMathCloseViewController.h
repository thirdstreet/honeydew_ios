//
//  DEWMathCloseViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/17/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DEWMethodCloseViewController.h"

@interface DEWMathCloseViewController : DEWMethodCloseViewController<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *mathProblemTextField;
@property (weak, nonatomic) IBOutlet UITextField *answerTextField;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

- (IBAction)addNumber:(id)sender;

- (IBAction)closeIt:(id)sender;
- (IBAction)cancelClose:(id)sender;

@end
