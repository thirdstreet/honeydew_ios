//
//  Honeydew.m
//  Honeydew
//
//  Created by Damon Floyd on 9/25/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "Honeydew.h"


@implementation Honeydew

@dynamic alert;
@dynamic closeMethod;
@dynamic closeTime;
@dynamic createTime;
@dynamic dewDescription;
@dynamic dueTime;
@dynamic nagEscalateTo;
@dynamic nagInterval;
@dynamic nagThreshold;
@dynamic notificationId;
@dynamic nagsSent;
@dynamic nagsScheduled;

- (BOOL)wasClosedLate
{
    //NSLog(@"closeTime: %f, dueTime: %f", self.closeTime, self.dueTime);
    if( self.closeTime != 0)
    {
        return self.closeTime > self.dueTime;
    }
    return NO;
}

- (int)lateImminentOrOkay
{
    
    NSDate *dueDateTime = [NSDate dateWithTimeIntervalSinceReferenceDate:self.dueTime];
    NSDate *now = [NSDate date];
    if([now compare:dueDateTime] == NSOrderedDescending) {
        return 1;
    }
    
    long alertSeconds = self.alert * 60;
    NSTimeInterval difference = [now timeIntervalSinceDate:dueDateTime];
    long late = alertSeconds + difference;
    
    if((late > 0) && (late < alertSeconds)) {
        // IMMINENT!!
        return 0;
    }
    
    // we're good
    return -1;
}

@end
