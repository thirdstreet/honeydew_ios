//
//  DEWInsultAccolade.h
//  Honeydew
//
//  Created by Damon Floyd on 9/24/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DEWInsultAccolade : NSObject

+ (NSString *)getInsult;
+ (NSString *)getAccolade;

@end
