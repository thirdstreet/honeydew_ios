//
//  DEWDetailViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/24/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Honeydew.h"

@interface DEWDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *youDidItAgoLabel;
@property (weak, nonatomic) IBOutlet UILabel *didTimestampLabel;
@property (weak, nonatomic) IBOutlet UILabel *ItWasDewdLabel;
@property (weak, nonatomic) IBOutlet UILabel *insultAccoladeLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdAgoLabel;
@property (weak, nonatomic) IBOutlet UILabel *createdTimestampLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueAgoLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueTimestampLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedViaLabel;

@property (weak, nonatomic) IBOutlet UIButton *statsButton;
- (IBAction)viewStats:(id)sender;

@property (strong, nonatomic) Honeydew *dew;

@end
