//
//  DEWCodeCloseViewController.m
//  Honeydew
//
//  Created by Damon Floyd on 9/17/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWCodeCloseViewController.h"
#import "DEWColors.h"

#define CODE_LENGTH 8

@interface DEWCodeCloseViewController ()
{
    NSString *_code;
}
@end

@implementation DEWCodeCloseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _code = [self generateCode];
    self.codeTextField.text = _code;
    
    self.closeButton.backgroundColor = [DEWColors honeydew];
    self.closeButton.tintColor = [UIColor whiteColor];
    
    self.cancelButton.backgroundColor = [DEWColors reallyLightGray];
    self.cancelButton.tintColor = [UIColor blackColor];
    
    self.errorLabel.textColor = [DEWColors lateRed];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextField delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //NSLog(@"did begin...");
    /*
    if(![_code isEqualToString:self.userCodeTextField.text]) {
        self.errorLabel.text = @"Codes do not match";
    }
    */
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    /*
    NSLog(@"did end...");
    if(![_code isEqualToString:self.userCodeTextField.text]) {
        self.errorLabel.text = @"Codes do not match";
    }
    else {
        self.errorLabel.text = @"";
    }
    */
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSLog(@"%@", self.userCodeTextField.text);
    if(![_code isEqualToString:[self.userCodeTextField.text stringByAppendingString:string]]) {
        self.errorLabel.text = @"Codes do not match";
    }
    else {
        self.errorLabel.text = @"";
    }
    
    return YES;
}

- (NSString *)generateCode
{
    static NSString *letters = @"abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
    NSMutableString *code = [NSMutableString stringWithCapacity:CODE_LENGTH];
    for(int i = 0; i < CODE_LENGTH; i++)
    {
        [code appendFormat:@"%C", [letters characterAtIndex:arc4random() % [letters length]]];
    }
    
    return code;
}

- (IBAction)closeIt:(id)sender
{
    //NSLog(@"closeIt");
    if([_code isEqualToString:self.userCodeTextField.text]) {
        //NSLog(@"codes match.  Calling delegate from Code close...");
        [self.delegate dewMethodViewController:self didCloseDew:YES];
    }
}

- (IBAction)cancelClose:(id)sender
{
    //NSLog(@"cancelClose");
    [self.delegate dewMethodViewController:self didCloseDew:NO];
}
@end
