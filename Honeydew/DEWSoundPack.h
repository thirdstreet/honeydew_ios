//
//  DEWSoundPack.h
//  Honeydew
//
//  Created by Damon Floyd on 9/24/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DEWSoundPack : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSDictionary *sounds;

- (void)loadWithName: (NSString *)name;


@end
