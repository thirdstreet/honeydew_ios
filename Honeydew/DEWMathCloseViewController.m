//
//  DEWMathCloseViewController.m
//  Honeydew
//
//  Created by Damon Floyd on 9/17/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWMathCloseViewController.h"
#import "DEWColors.h"

@interface DEWMathCloseViewController ()
{
    NSString *_equation;
    NSString *_correctAnswer;
    
}
@end

@implementation DEWMathCloseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupEquation];
    self.mathProblemTextField.text = _equation;
    
    self.closeButton.backgroundColor = [DEWColors honeydew];
    self.closeButton.tintColor = [UIColor whiteColor];
    
    self.cancelButton.backgroundColor = [DEWColors reallyLightGray];
    self.cancelButton.tintColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupEquation
{
    NSArray *operators = @[@"+", @"-", @"/", @"x"];
    NSString *operator = operators[(arc4random() % operators.count)];
    //NSString *operator = @"x";
    int op1, op2, answer;
    op1 = op2 = answer = 0;
    
    if([operator isEqualToString:@"+"]) {
        op1 = (arc4random() % 45) + 5;
        op2 = (arc4random() % 45) + 5;
        answer = op1 + op2;
    }
    if([operator isEqualToString:@"-"]) {
        op1 = (arc4random() % 90) + 10;
        op2 = (arc4random() % op1);
        answer = op1 - op2;
    }
    if([operator isEqualToString:@"x"]) {
        op1 = (arc4random() % 12) + 1;
        op2 = (arc4random() % 12) + 1;
        answer = op1 * op2;
    }
    if([operator isEqualToString:@"/"]) {
        op2 = (arc4random() % 12) + 1;
        answer = (arc4random() % 12) + 1;
        op1 = op2 * answer;
    }
    _equation = [NSString stringWithFormat:@"%d %@ %d", op1, operator, op2];
    _correctAnswer = [NSString stringWithFormat:@"%d", answer];
    //NSLog(@"%@ = %@", _equation, _correctAnswer);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Alert View
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //clear the answer out - they got it wrong
    self.answerTextField.text = @"";
}

#pragma mark - Number Buttons
- (IBAction)addNumber:(id)sender
{
    if([self.answerTextField.text length] < 3) {
        UIButton *button = (UIButton *)sender;
        NSString *number = button.titleLabel.text;
        //NSLog(@"number: %@", number);
        if([self.answerTextField.text length] == 0) {
            //NSLog(@"text field length is zero....");
            // can't start with a 0
            if(![number isEqualToString:@"0"]) {
                //NSLog(@"adding %@ since it's not a zero...", number);
                [self addToAnswer:number];
            }
        }
        else {
            //NSLog(@"answer length is over zero.  Adding...");
            [self addToAnswer:number];
        }
    }
}

- (void) addToAnswer:(NSString *)number
{
    self.answerTextField.text = [self.answerTextField.text stringByAppendingString:number];
}

#pragma mark - Other Buttons

- (IBAction)closeIt:(id)sender
{
    if ([self.answerTextField.text isEqualToString:_correctAnswer]) {
        [self.delegate dewMethodViewController:self didCloseDew:YES];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Um. No."
                                                        message:@"That answer is incorrect"
                                                       delegate:self
                                              cancelButtonTitle:@"Oh my bad."
                                              otherButtonTitles:nil];
    
        [alert show];
    }
}

- (IBAction)cancelClose:(id)sender
{
    [self.delegate dewMethodViewController:self didCloseDew:NO];
}



@end
