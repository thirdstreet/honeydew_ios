//
//  DEWCloseViewController.m
//  Honeydew
//
//  Created by Damon Floyd on 9/16/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWCloseViewController.h"
#import "DEWColors.h"
#import "DEWAppDelegate.h"
#import "YLMoment.h"

@interface DEWCloseViewController ()
{
    NSArray *_harassments;
    NSArray *_accolades;
    NSDateFormatter *_dateFormatter;
    UIColor *_accentColor;
}
@end

@implementation DEWCloseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupInsultsAccolades];
    
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"yyyy-MM-dd @ HH:mm"];
    
    _accentColor = [self getColorForStatus];
    
    self.dewDescriptionLabel.text = self.honeydew.dewDescription;
    
    self.dueElapsedTimeLabel.attributedText = [self getDueTimeLabelText];
    self.dueDateStampLabel.text = [_dateFormatter stringFromDate:[self.honeydew valueForKey:@"dueTime"]];
    self.dueDateStampLabel.textColor = _accentColor;
    
    self.createElapsedTimeLabel.attributedText = [self getCreateTimeLabelText];
    self.createDateStampLabel.textColor = _accentColor;
    self.createDateStampLabel.text = [_dateFormatter stringFromDate:[self.honeydew valueForKey:@"createTime"]];
    
    self.insultAccoladeLabel.textColor = _accentColor;
    self.insultAccoladeLabel.text = [self getInsultAccolade];
    
    self.closeButton.tintColor = [DEWColors cantalope];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Text Formatting

- (NSString *)getInsultAccolade
{
    int status = [self.honeydew lateImminentOrOkay];
    if(status == 0) { status = -1; }
    
    if(status == 1) {
        // late
        int r = arc4random() % ([_harassments count]);
        return _harassments[r];
    }
    
    int r = arc4random() % [_accolades count];
    return _accolades[r];
}

- (NSAttributedString *)getDueTimeLabelText
{
    NSDate *dueTime = [self.honeydew valueForKey:@"dueTime"];
    NSString *momentString = [self getMomentStringForDate:dueTime];
    NSString *fullString = [NSString stringWithFormat:@"Due %@ on", momentString];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:fullString];
    [string addAttribute:NSForegroundColorAttributeName value:_accentColor range:NSMakeRange(4, [momentString length])];
    
    return string;
}

- (NSAttributedString *)getCreateTimeLabelText
{
    NSDate *createTime = [self.honeydew valueForKey:@"createTime"];
    NSString *momentString = [self getMomentStringForDate:createTime];
    NSString *fullString = [NSString stringWithFormat:@"Created %@ on", momentString];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:fullString];
    [string addAttribute:NSForegroundColorAttributeName value:_accentColor range:NSMakeRange(8, [momentString length])];
    
    return string;
}

- (NSString *)getMomentStringForDate:(NSDate *)date
{
    YLMoment *moment = [[YLMoment alloc]initWithDate:date];
    return [moment fromDate:[NSDate date]];
}

- (UIColor *)getColorForStatus
{
    int status = [self.honeydew lateImminentOrOkay];
    switch(status) {
        case -1:
            return [DEWColors honeydew];
        case 0:
            return [DEWColors cantalope];
        case 1:
            return [DEWColors lateRed];
    }
    // should never get here...
    return [DEWColors honeydew];
}

#pragma mark - Setup

-(void)setupInsultsAccolades
{
    _harassments = @[@"You're late on this one, bucko!",
                      @"Way to go.  And by 'way to go' I mean way to fail.",
                      @"Late. Late. Late. Laaaaaaaate.",
                      @"Are we having trouble with task completion?",
                      @"Time management doesn't seem to be your strong suit.",
                      @"uh oh!  Late!",
                      @"You're late!  You're late!  For a very important date!",
                      @"late again?",
                      @"Late?  You're not filling me with confidence.",
                      @"Doing stuff on time is hard."];
    
    _accolades = @[@"Awww yeah.  Closing it early!",
                   @"Nice job!",
                   @"Way to go, kid!",
                   @"I will speak nicer of you in the future.",
                   @"Awesome.  This is awesome.  You're awesome.",
                   @"Wut wut?  Can you FEEL IT?!?",
                   @"Look a little smug.  You earned it.",
                   @"What was that?  I can't hear over the sound of how awesome you are.",
                   @"Commence victory dance!"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeThisDew:(id)sender
{
    NSString *method = [self.honeydew valueForKey:@"closeMethod"];
    //NSLog(@"method: %@", method);
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if([method isEqualToString:@"code"]) {
        DEWCodeCloseViewController *vc = (DEWCodeCloseViewController *)[storyboard instantiateViewControllerWithIdentifier:@"codeClose"];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        //vc.modalTransitionStyle = UIModalTransitionStylePartialCurl;
        //vc.view.backgroundColor = [DEWColors honeydew];
        vc.delegate = self;
        [self.navigationController presentViewController:vc animated:YES completion:nil];
    }
    
    if([method isEqualToString:@"math"]) {
        DEWMathCloseViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"mathClose"];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        //vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        //vc.view.backgroundColor = [DEWColors honeydew];
        vc.delegate = self;
        [self.navigationController presentViewController:vc animated:YES completion:nil];
    }
    
    if([method isEqualToString:@"puzzle"]) {
        DEWPuzzleCloseViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"puzzleClose"];
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        //vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        //vc.view.backgroundColor = [DEWColors honeydew];
        vc.delegate = self;
        [self.navigationController presentViewController:vc animated:YES completion:nil];
    }
    
}

- (void)dewMethodViewController:(DEWMethodCloseViewController *)controller didCloseDew:(BOOL)closed
{
    if(closed) {
        self.honeydew.closeTime = [[NSDate date] timeIntervalSinceReferenceDate];
        
        // unschedule any remaining notifications
        int unscheduledNags = [self unscheduleNotifications];
        
        // compute the number of nags we actually sent.
        int totalSent = (self.honeydew.nagsScheduled - unscheduledNags);
        //NSLog(@"scheduled: %d, unscheduled: %d", self.honeydew.nagsScheduled, unscheduledNags);
        if (totalSent < 0) {
            totalSent = 0;
        }
        self.honeydew.nagsSent = totalSent;
        
        DEWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSError *error;
        [context save:&error];
        // dismiss modal
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        //NSLog(@"closed.  Calling delegate from close Detail...");
        [self.delegate dewCloseViewController:self didCloseDew:YES];
    }
    else {
        // dismiss modal
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
}

#pragma mark - Notifications
- (int) unscheduleNotifications
{
    int unscheduled = 0;
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *allNotifications = [app scheduledLocalNotifications];
    for(int i = 0; i < [allNotifications count]; i++)
    {
        UILocalNotification *n = [allNotifications objectAtIndex:i];
        NSDictionary *userInfo = n.userInfo;
        NSString *uuid = [NSString stringWithFormat:@"%@", [userInfo valueForKey:@"id"]];
        if( [self.honeydew.notificationId isEqualToString:uuid])
        {
            [app cancelLocalNotification:n];
            unscheduled++;
        }
    }
    
    //NSLog(@"unscheduled %d remaining notifications for this 'dew", unscheduled);
    return unscheduled;
}

@end
