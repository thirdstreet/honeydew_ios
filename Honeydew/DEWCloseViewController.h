//
//  DEWCloseViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/16/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Honeydew.h"
#import "DEWMethodCloseViewController.h"
#import "DEWCodeCloseViewController.h"
#import "DEWMathCloseViewController.h"
#import "DEWPuzzleCloseViewController.h"

@class DEWCloseViewController;

@protocol DEWCloseViewControllerDelegate <NSObject>

- (void) dewCloseViewController: (DEWCloseViewController *)controller didCloseDew: (BOOL) closed;

@end

@interface DEWCloseViewController : UIViewController<DEWMethodCloseViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *dewDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueElapsedTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueDateStampLabel;
@property (weak, nonatomic) IBOutlet UILabel *createElapsedTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *createDateStampLabel;
@property (weak, nonatomic) IBOutlet UILabel *insultAccoladeLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

- (IBAction)closeThisDew:(id)sender;

@property (weak, nonatomic) Honeydew *honeydew;
@property (weak, nonatomic) id<DEWCloseViewControllerDelegate> delegate;

- (void)dewMethodViewController:(DEWMethodCloseViewController *)controller didCloseDew:(BOOL)closed;

@end
