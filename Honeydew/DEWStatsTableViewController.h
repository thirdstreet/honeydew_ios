//
//  DEWStatsTableViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/25/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DEWStatsTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *totalClosedDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCreatedDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedOnTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedOnTimeDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedLateLabel;
@property (weak, nonatomic) IBOutlet UILabel *closedLateDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *battingAverageDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesNaggedDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesFirmerLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesFirmerDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesInsistentLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesInsistentDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesFussyLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesFussyDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesAngryLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTimesAngryDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageDewLengthDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageCloseTimeDetailLabel;
@property (weak, nonatomic) IBOutlet UILabel *efficiencyRatingDetailLabel;

@end
