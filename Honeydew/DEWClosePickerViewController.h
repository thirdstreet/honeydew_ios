//
//  DEWClosePickerViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/10/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DEWClosePickerViewController;

@protocol ClosePickerViewControllerDelegate <NSObject>

- (void)closeMethodPickerViewController:(DEWClosePickerViewController *)controller didSelectCloseMethod:(NSString *)closeMethod;

@end

@interface DEWClosePickerViewController : UITableViewController

@property (nonatomic, weak) id <ClosePickerViewControllerDelegate> delegate;
@property (nonatomic, strong) NSString *closeMethod;

@end
