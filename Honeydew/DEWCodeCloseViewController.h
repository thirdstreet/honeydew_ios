//
//  DEWCodeCloseViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/17/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DEWMethodCloseViewController.h"

@interface DEWCodeCloseViewController : DEWMethodCloseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *userCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;


- (IBAction)closeIt:(id)sender;
- (IBAction)cancelClose:(id)sender;

@end
