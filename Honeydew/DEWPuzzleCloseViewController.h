//
//  DEWPuzzleCloseViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 10/17/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DEWMethodCloseViewController.h"

@interface DEWPuzzleCloseViewController : DEWMethodCloseViewController
@property (weak, nonatomic) IBOutlet UIButton *solution0;
@property (weak, nonatomic) IBOutlet UIButton *solution1;
@property (weak, nonatomic) IBOutlet UIButton *solution2;
@property (weak, nonatomic) IBOutlet UIButton *solution3;
@property (weak, nonatomic) IBOutlet UIButton *puzzle0;
@property (weak, nonatomic) IBOutlet UIButton *puzzle1;
@property (weak, nonatomic) IBOutlet UIButton *puzzle2;
@property (weak, nonatomic) IBOutlet UIButton *puzzle3;

@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

- (IBAction)puzzleButtonTap:(id)sender;
- (IBAction)closeIt:(id)sender;
- (IBAction)cancelClose:(id)sender;
@end
