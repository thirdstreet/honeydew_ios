//
//  DEWAlertPickerViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/10/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DEWAlertPickerViewController;

@protocol AlertPickerViewControllerDelegate <NSObject>

- (void)alertPickerViewController:(DEWAlertPickerViewController *)controller didSelectAlert:(NSString *)alert;

@end

@interface DEWAlertPickerViewController : UITableViewController

@property (nonatomic, weak) id <AlertPickerViewControllerDelegate> delegate;
@property (nonatomic, strong) NSString *alert;

@end
