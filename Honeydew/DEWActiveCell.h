//
//  DEWActiveCellTableViewCell.h
//  Honeydew
//
//  Created by Damon Floyd on 9/15/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DEWActiveCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *tagImageView;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UILabel *dueTimeLabel;


@end
