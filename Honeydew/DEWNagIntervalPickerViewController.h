//
//  DEWNagIntervalPickerViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/11/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DEWNagIntervalPickerViewController;

@protocol NagIntervalPickerViewControllerDelegate <NSObject>

- (void) nagIntervalPickerController: (DEWNagIntervalPickerViewController *)controller didSelectNagInterval: (NSString *)nagInterval;

@end

@interface DEWNagIntervalPickerViewController : UITableViewController

@property (nonatomic, weak) id <NagIntervalPickerViewControllerDelegate> delegate;
@property (nonatomic, strong) NSString *nagInterval;

@end
