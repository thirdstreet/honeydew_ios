//
//  DEWStatsTableViewController.m
//  Honeydew
//
//  Created by Damon Floyd on 9/25/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWStatsTableViewController.h"
#import "DEWColors.h"
#import "DEWInsultAccolade.h"
#import "DEWAppDelegate.h"
#import "YLMoment.h"
#import "Honeydew.h"

@interface DEWStatsTableViewController ()
{
    NSMutableArray *_active;
    NSMutableArray *_closed;
}
@end

@implementation DEWStatsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLabelColors];
    
    _active = [[NSMutableArray alloc] init];
    _closed = [[NSMutableArray alloc] init];
    
    [self loadData];
    [self computeStats];
}

#pragma mark - Data
- (void)computeStats
{
    [self computeTotals];
    [self computeEfficiency];
}

- (void)computeEfficiency
{
    unsigned long dewCount = [_closed count];
    NSTimeInterval totalLength = 0;
    NSTimeInterval totalCloseTime = 0;
    
    NSTimeInterval mostEfficientClose = DBL_MAX;
    NSTimeInterval mostEfficientLength = 0;
    NSTimeInterval leastEfficientClose = 0;
    NSTimeInterval leastEfficientLength = 0;
    
    for (int i = 0; i < [_closed count]; i++) {
        Honeydew *hd = [_closed objectAtIndex:i];
        NSTimeInterval length = hd.dueTime - hd.createTime;
        NSTimeInterval closeTime = hd.closeTime - hd.createTime;
        //NSLog(@"length: %f, close: %f", length, closeTime);
        totalLength += length;
        totalCloseTime += closeTime;
        //NSLog(@"totalLength: %f, totalCloseTime: %f", totalLength, totalCloseTime);
        if (closeTime < mostEfficientClose) {
            mostEfficientClose = closeTime;
            mostEfficientLength = length;
        }
        
        if (closeTime > leastEfficientClose) {
            leastEfficientClose = closeTime;
            leastEfficientLength = length;
        }
    }
    
    NSTimeInterval avgLength = totalLength / dewCount;
    NSTimeInterval avgCloseTime = totalCloseTime / dewCount;
    
    float efficiencyRating = 100.0f- ((avgCloseTime / avgLength) * 100.0f);
    
    NSString *lengthStr = [self getLengthStringFromInterval:avgLength];
    NSString *closeLengthStr = [self getLengthStringFromInterval:avgCloseTime];
    
    self.averageDewLengthDetailLabel.text = lengthStr;
    self.averageCloseTimeDetailLabel.text = closeLengthStr;
    self.efficiencyRatingDetailLabel.text = [NSString stringWithFormat:@"%2.2f %%", efficiencyRating];
    self.efficiencyRatingDetailLabel.textColor = [DEWColors honeydew];
    if (efficiencyRating <= 70.0f) {
        self.efficiencyRatingDetailLabel.textColor = [DEWColors cantalope];
    } else if (efficiencyRating <= 25.0f) {
        self.efficiencyRatingDetailLabel.textColor = [DEWColors lateRed];
    }
}

- (void)computeTotals
{
    self.totalCreatedDetailLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)([_active count] + [_closed count])];
    self.totalClosedDetailLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[_closed count]];
    int onTime = 0;
    int late = 0;
    int totalNags = 0;
    int totalFirmer = 0;
    int totalInsistent;
    int totalFussy = 0;
    int totalAngry = 0;
    
    for(int i = 0; i < [_closed count]; i++)
    {
        Honeydew *hd = [_closed objectAtIndex:i];
        if([hd wasClosedLate]) {
            late++;
        }
        else {
            onTime++;
        }
        //NSLog(@"sent: %d", hd.nagsSent);
        totalNags += hd.nagsSent;
        if ([hd.nagEscalateTo isEqualToString:@"firmer"]) {
            if (hd.nagsSent > hd.nagThreshold) {
                totalFirmer += (hd.nagsSent - hd.nagThreshold);
            }
        } else if ([hd.nagEscalateTo isEqualToString:@"insistent"]) {
            if (hd.nagsSent > hd.nagThreshold) {
                totalInsistent += (hd.nagsSent - hd.nagThreshold);
            }
        } else if ([hd.nagEscalateTo isEqualToString:@"fussy"]) {
            if (hd.nagsSent > hd.nagThreshold) {
                totalFussy += (hd.nagsSent - hd.nagThreshold);
            }
        } else {
            if (hd.nagsSent > hd.nagThreshold) {
                totalAngry += (hd.nagsSent - hd.nagThreshold);
            }
        }
        
    }
    
    self.totalTimesNaggedDetailLabel.text = [NSString stringWithFormat:@"%d", totalNags];
    self.totalTimesAngryDetailLabel.text = [NSString stringWithFormat:@"%d", totalAngry];
    self.totalTimesFirmerDetailLabel.text = [NSString stringWithFormat:@"%d", totalFirmer];
    self.totalTimesFussyDetailLabel.text = [NSString stringWithFormat:@"%d", totalFussy];
    self.totalTimesInsistentDetailLabel.text = [NSString stringWithFormat:@"%d", totalInsistent];
    
    self.closedLateDetailLabel.text = [NSString stringWithFormat:@"%d", late];
    self.closedOnTimeDetailLabel.text = [NSString stringWithFormat:@"%d", onTime];
    
    float battingAverage = (float)onTime / [_closed count];
    
    self.battingAverageDetailLabel.text = [NSString stringWithFormat:@"%.3f", battingAverage];
    self.battingAverageDetailLabel.textColor = [DEWColors honeydew];
    if (battingAverage < 0.750f) {
        self.battingAverageDetailLabel.textColor = [DEWColors cantalope];
    }
    if (battingAverage < 0.251f) {
        self.battingAverageDetailLabel.textColor = [DEWColors lateRed];
    }
}

- (void)loadData
{
    DEWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Honeydew" inManagedObjectContext:context];
    
    NSFetchRequest *req = [[NSFetchRequest alloc] init];
    [req setEntity:entity];
    
    // fetch active 'dews
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(closeTime = nil)"];
    [req setPredicate:pred];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"dueTime" ascending:YES];
    [req setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    NSManagedObject *active = nil;
    NSError *error;
    
    NSArray *objects = [context executeFetchRequest:req error:&error];
    
    if ([objects count] == 0) {
        // do nothing.  The title for the secton is handled by the table delegate above
    }
    else {
        for(int i = 0; i < [objects count]; i++)
        {
            active = objects[i];
            [_active addObject:active];
        }
    }
    
    pred = nil;
    pred = [NSPredicate predicateWithFormat:@"(closeTime != nil)"];
    sort = nil;
    sort = [[NSSortDescriptor alloc] initWithKey:@"closeTime" ascending:NO];
    [req setPredicate:pred];
    [req setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    
    NSManagedObject *recent = nil;
    objects = nil;
    objects = [context executeFetchRequest:req error:&error];
    if ([objects count] == 0) {
        // do nothing.  The section header retitle is handled by the table delegate above
    }
    else {
        for (int i = 0; i < [objects count]; i++)
        {
            recent = objects[i];
            [_closed addObject:recent];
        }
    }
}

#pragma mark - Cosmetics
- (void)setupLabelColors
{
    self.totalClosedDetailLabel.textColor = [DEWColors honeydew];
    self.totalCreatedDetailLabel.textColor = [DEWColors honeydew];
    self.closedOnTimeDetailLabel.textColor = [DEWColors honeydew];
    self.closedLateDetailLabel.textColor = [DEWColors lateRed];
    self.totalTimesNaggedDetailLabel.textColor = [DEWColors cantalope];
    self.totalTimesFirmerDetailLabel.textColor = [DEWColors cantalope];
    self.totalTimesInsistentDetailLabel.textColor = [DEWColors cantalope];
    self.totalTimesFussyDetailLabel.textColor = [DEWColors cantalope];
    self.totalTimesAngryDetailLabel.textColor = [DEWColors cantalope];
    
    // attributed Labels
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:@"Closed On Time:"];
    [attrString addAttribute:NSForegroundColorAttributeName value:[DEWColors honeydew] range:NSMakeRange(7, 7)];  // colors 'On Time'
    self.closedOnTimeLabel.attributedText = attrString;
    
    attrString = [[NSMutableAttributedString alloc] initWithString:@"Closed Late:"];
    [attrString addAttribute:NSForegroundColorAttributeName value:[DEWColors lateRed] range:NSMakeRange(7, 4)];  // colors 'Late'
    self.closedLateLabel.attributedText = attrString;
    
    attrString = [[NSMutableAttributedString alloc] initWithString:@"Total Times I Got Firmer:"];
    [attrString addAttribute:NSForegroundColorAttributeName value:[DEWColors cantalope] range:NSMakeRange(18, 6)];  // 'Firmer'
    self.totalTimesFirmerLabel.attributedText = attrString;
    
    attrString = [[NSMutableAttributedString alloc] initWithString:@"Total Times I Got Insistent:"];
    [attrString addAttribute:NSForegroundColorAttributeName value:[DEWColors cantalope] range:NSMakeRange(18, 9)];  // 'Insistent'
    self.totalTimesInsistentLabel.attributedText = attrString;
    
    attrString = [[NSMutableAttributedString alloc] initWithString:@"Total Times I Got Fussy:"];
    [attrString addAttribute:NSForegroundColorAttributeName value:[DEWColors cantalope] range:NSMakeRange(18, 5)];  // 'Fussy'
    self.totalTimesFussyLabel.attributedText = attrString;
    
    attrString = [[NSMutableAttributedString alloc] initWithString:@"Total Times I Got Angry:"];
    [attrString addAttribute:NSForegroundColorAttributeName value:[DEWColors cantalope] range:NSMakeRange(18, 5)];  // 'Angry'
    self.totalTimesAngryLabel.attributedText = attrString;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Relative Time Formatting

- (NSString *)getLengthStringFromInterval:(NSTimeInterval )interval
{
    
    int seconds = (int)interval;
    int minutes = 0;
    int hours = 0;
    int days = 0;
    int weeks = 0;
    
    NSMutableString *outString = [[NSMutableString alloc] init];
    if(seconds >= 60) {
        minutes = seconds / 60;
        seconds = seconds % 60;
    }
    
    if (minutes >= 60) {
        hours = minutes / 60;
        minutes = minutes % 60;
    }
    
    if (hours >= 24) {
        days = hours / 24;
        hours = hours % 24;
    }
    
    if (days >= 7) {
        weeks = days / 7;
        days = days % 7;
    }
    
    if (weeks > 3) {
        return @"Weeks.  Seriously.";
    }
    
    if(weeks > 0) {
        //NSString *weekStr = weeks == 1 ? @"week" : @"weeks";
        [outString appendFormat:@"%dw ", weeks];
    }
    
    if (days > 0) {
//        NSString *dayStr = days == 1 ? @"day" : @"days";
        [outString appendFormat:@"%dd ", days];
    }
    
    if (hours > 0) {
//        NSString *hourStr = hours == 1 ? @"hour" : @"hours";
        [outString appendFormat:@"%dh ", hours];
    }
    
    if (minutes > 0) {
//        NSString *minStr = minutes == 1 ? @"minute" : @"minutes";
        [outString appendFormat:@"%dm ", minutes];
    }
    
    if ([_closed count] < 3) {
        // show seconds
        if (seconds > 0) {
//            NSString *secStr = seconds == 1 ? @"second" : @"seconds";
            [outString appendFormat:@"%ds", seconds];
        }
    }
    
    if ([outString isEqualToString:@""]) {
        [outString appendString:@"< 1 minute"];
    }
    
    return outString;
    
}

- (float)computeEfficiencyRating: (double )part Whole:(double )whole
{
    return 0.0f;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0) {
        return 8;
    }
    return 5;
}


@end
