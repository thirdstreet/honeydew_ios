//
//  DEWSoundPack.m
//  Honeydew
//
//  Created by Damon Floyd on 9/24/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWSoundPack.h"

@implementation DEWSoundPack
{
    NSArray *_keys;
    NSArray *_wavs;
    NSDictionary *_defaults;
}

- (id)init
{
    self = [super init];
    if(self) {
        _wavs = @[@"alert.wav", @"due.wav", @"firmer.wav", @"insistent.wav", @"fussy.wav", @"angry.wav"];
        _keys = @[@"alert", @"due", @"firmer", @"insistent", @"fussy", @"angry"];
        
        _defaults = [NSDictionary dictionaryWithObjects:_wavs forKeys:_keys];
    }
    
    return self;
}

- (void)loadWithName:(NSString *)name
{
    NSMutableDictionary *soundIndex = [[NSMutableDictionary alloc] init];
    
    NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *folder = [docsPath stringByAppendingPathComponent:name];
    NSFileManager *fileManager = [NSFileManager defaultManager];

    if ([fileManager fileExistsAtPath:folder]) {
        // the folder is there.  Check for the files...
        NSString *keyName;
        for (keyName in _keys) {
            NSString *soundName = [NSString stringWithFormat:@"%@.wav", keyName];
            NSString *soundPath = [folder stringByAppendingPathComponent:soundName];
            if ([fileManager fileExistsAtPath:soundPath]) {
                [soundIndex setValue:soundPath forKey:keyName];
            }
            else {
                [soundIndex setValue:soundName forKey:keyName];
            }
        }
        self.sounds = soundIndex;
    }
    else {
        // that pack does not exist...
        self.sounds = _defaults;
    }
}

@end
