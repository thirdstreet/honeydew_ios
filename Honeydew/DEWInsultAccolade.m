//
//  DEWInsultAccolade.m
//  Honeydew
//
//  Created by Damon Floyd on 9/24/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWInsultAccolade.h"

@implementation DEWInsultAccolade


/*
 
 
 
*/

+ (NSString *)getInsult
{
    NSArray *_insults = @[@"You're late on this one, bucko!",
                              @"Way to go.  And by 'way to go' I mean way to fail.",
                              @"Late. Late. Late. Laaaaaaaate.",
                              @"Are we having trouble with task completion?",
                              @"Time management doesn't seem to be your strong suit.",
                              @"uh oh!  Late!",
                              @"You're late!  You're late!  For a very important date!",
                              @"late again?",
                              @"Late?  You're not filling me with confidence.",
                              @"Doing stuff on time is hard."];
    
    return [_insults objectAtIndex:(arc4random() % [_insults count])];
}

+ (NSString *)getAccolade
{
    NSArray *_accolades = @[@"Awww yeah.  Closing it early!",
                   @"Nice job!",
                   @"Way to go, kid!",
                   @"I will speak nicer of you in the future.",
                   @"Awesome.  This is awesome.  You're awesome.",
                   @"Wut wut?  Can you FEEL IT?!?",
                   @"Look a little smug.  You earned it.",
                   @"What was that?  I can't hear over the sound of how awesome you are.",
                   @"Commence victory dance!"];
    
    return [_accolades objectAtIndex:(arc4random() % [_accolades count])];
}

@end
