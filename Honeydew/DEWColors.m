//
//  DEWColors.m
//  Honeydew
//
//  Created by Damon Floyd on 9/9/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import "DEWColors.h"

@implementation DEWColors

+ (UIColor *) honeydew
{
    return [UIColor colorWithRed:165/256.0 green:214/256.0 blue:128 / 256.0 alpha:1.0f];
    //return [UIColor colorWithHue:(94.0f / 360.0f) saturation:0.51f brightness:0.67f alpha:1.0f];
}

+ (UIColor *) cantalope
{
    return [UIColor colorWithRed:240/256.0 green:138/256.0 blue:122/256.0 alpha:1.0f];
}

+ (UIColor *) lateRed
{
    return [UIColor colorWithRed:188/256.0 green:43/256.0 blue:21/256.0 alpha:1.0f];
}

+ (UIColor *) reallyLightGray
{
    return [UIColor colorWithRed:0.96f green:0.96f blue:0.96f alpha:1.0f];
}

@end
