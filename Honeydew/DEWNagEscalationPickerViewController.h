//
//  DEWNagEscalationPickerViewController.h
//  Honeydew
//
//  Created by Damon Floyd on 9/11/14.
//  Copyright (c) 2014 Third Street Soft. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DEWNagEscalationPickerViewController;

@protocol NagEscalationPickerDelegate <NSObject>

- (void)nagEscalationPickerController: (DEWNagEscalationPickerViewController *)controller didSelectEscalationLevel: (NSString *)escalationLevel;

@end

@interface DEWNagEscalationPickerViewController : UITableViewController

@property (nonatomic, weak) id <NagEscalationPickerDelegate> delegate;
@property (nonatomic, strong) NSString *nagEscalationLevel;

@end
